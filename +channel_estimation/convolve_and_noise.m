function r_m = convolve_and_noise(s,H_l,SNR, N)
%convolve_and_noise convolves each of the symbols with the corresponding
%channel realization - combines all into an overlapping S*N+L-1 vector
%   SNR linear here

    [S, L] = size(H_l);
    N = length(s(1,:));

    R_m = zeros(S,L+N-1);
    for i=1:S
        noise = 1/sqrt(SNR) * (normrnd(0,1/sqrt(2),[1,L+N-1]) + 1j * normrnd(0,1/sqrt(2), [1,L+N-1]));
        R_m(i,:) = conv(s(i,:), H_l(i,:)) + noise;
    end

    %length is L+N-1 and it should fit within L values

    %For offset this should be a single vector -> combine again
    r_m = zeros(1,S*N+L-1);
    for i=1:S
        r_m((i-1)*N+1:i*N+L-1) = r_m((i-1)*N+1:i*N+L-1) + R_m(i,:);
    end

end



