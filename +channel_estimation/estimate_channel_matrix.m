function estimated_channel = estimate_channel_matrix(y, pilot_mask, S, K, L)
%estimate_channel_matrix Summary of this function goes here
%   Detailed explanation goes here
    estimated_channel = NaN(S,K);
    estimated_channel(logical(pilot_mask)) = y(logical(pilot_mask)) / qammod(8,16) * sqrt(10);
    for i=1:S
        nanx = isnan(estimated_channel(i,:));
        indices = 1:K;
        estimated_channel(i,:) = interp1(indices(~nanx), estimated_channel(i,~nanx), 1:K, 'spline');

        %Rank Reduction of channel
        
        %old_channel = abs(estimated_channel(i,:));
        h_l_est = ifft(estimated_channel(i,:)); %length K instead of length L
        h_l_est(L:end) = 0;
        estimated_channel(i,:) = fft(h_l_est);
        
    end
end

