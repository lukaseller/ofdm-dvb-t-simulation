function [pilotMask, nDataCarriers] = generate_pilot_mask(S,K)
% This function generates a logical Pilot Mask according to ETSI EN 300 744
% Input: 
%       S             ... Number of Symbols in time
%       K             ... Number of Subcarriers
% Output:
%       pilotMask     ... SxK logical with 1 at pilot positions
%       nDataCarriers ... Number of data subcarriers

pilotMask = false(S,K);

for l=1:S
    
    %From the standard: k=Kmin+3???(lmod4)+12p pinteger,p?0,k?[Kmin;Kmax]
    pilotPositions = 3*mod(l,4)+12*(0:fix(K/12)); %use 12!
    pilotMask(l,pilotPositions(pilotPositions<K)+1) = true;
end

pilotMask(:,1) = true;
pilotMask(:,end) = true;


%Returns the total number of free data carriers
nDataCarriers = S*K - nnz(pilotMask);
end
