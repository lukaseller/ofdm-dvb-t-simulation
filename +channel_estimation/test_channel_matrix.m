function H_l = test_channel_matrix(S,L)
%test_channel_matrix returns a SxL matrix -> one L tap channel for each
%ofdm symbol
    H_l = zeros(S,L);
    for i=1:S
        H_l(i,:) = generate_test_channel(L);
    end
end

function h = generate_test_channel(L)
%generate_test_channel generates a length L test channel
    h = zeros(1, L);
    %h(1) = 1;
    pdp = exp(-(0:L)/2);
    for i=1:L
       h(i) = pdp(i) * (normrnd(0,1/sqrt(2)) + 1j * normrnd(0,1/sqrt(2)));
    end
    
    h = h * 100;
end