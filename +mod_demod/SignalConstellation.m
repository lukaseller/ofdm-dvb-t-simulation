function [alphabet, helpMatrix]= SignalConstellation
% This function returns a vector "alphabet" with length M = 16
% Values of "alphabet"  : complex symbol position coefficients

M = 16;
k = log2(M);
power=ceil(log2(sqrt(M)));
if M>2
    v1 = 2 ^ power;
    v2 = v1 - 1;
    vec = ((0:v2) - v2 / 2) * 2;
    qam = complex(repmat(vec, v1, 1),repmat(-transpose(vec), 1, v1));
    alphabet = reshape(qam, 1, []);
    
    
end

GrayBits = [0,1];
for n=1:k-1
    GrayBits = ([GrayBits fliplr(GrayBits)]);
    GrayBits = [zeros(1,2^(n+1)/2), ones(1,2^(n+1)/2); GrayBits];
end
%         imagesc(GrayBits)
n_t = sqrt(length(alphabet))-1;
for n=n_t+2:2*sqrt(length(alphabet)):length(alphabet)
    GrayBits(:,n:n+n_t) = fliplr(GrayBits(:,n:n+n_t));
end

helpMatrix = flipud(GrayBits);
grayIntegers = bi2de(helpMatrix.').';
alphabet = alphabet(grayIntegers+1);

% Scale the alphabet so the mean power equals one
meanPower = norm(alphabet)^2 / length(alphabet);

alphabet = alphabet/sqrt(meanPower);
end

% Plotting signal constellation with bit mapping
function PlotSignalConstellation
    scatter(real(alphabet),imag(alphabet),60,'filled')
    showMatrix = de2bi(0:size-1);
    for ii=1:length(alphabet)
        grid on
        hold on
        text(real(alphabet(ii))-0.15,imag(alphabet(ii))+0.09,num2str(showMatrix(ii,:)),'Fontsize',14)
        axis([-1.2,1.2,-1.2,1.2])
        set(gca,'fontsize',14,'Box','on')
    end
end

