function x = assign_subcarriers(symbol_stream, pilot_mask, pilot_index)
% This function combines the data symbol stream with the pilot pattern
% Inputs:      symbol_stream ... Symbol vector
%              pilot_mask    ... SxK Logical Pilot Mask
% Output:
%              x ... SxK Symbol Transmission Matrix

[S,K] = size(pilot_mask);

x = pilot_index*ones(S*K,1); %One frame of data. rows are ofdm-symbold, collumns are subcarriers

pilotMask = reshape(pilot_mask.',[],1);
x(~pilotMask) = symbol_stream.';
x = reshape(x,K,S).';

end
