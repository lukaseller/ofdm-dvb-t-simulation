function transmitSignal = modulate_ofdm(dataStream, L_cp ,FFTSize)
% This function performs an OFDM modulation and CP addition to the symbol stream.
% Inputs:
%          dataStream ... Symbol Matrix of size nSymbolsTime x nSubcarriers
%          L_cp       ... Cyclic Prefix length
%          FFTSize    ... FFT Size
    [S, K] = size(dataStream);
    dataSymbolsTemp = zeros(S,FFTSize);
    
    dataSymbolsTemp(:,1:K) = dataStream;

    TransmitSignalNoCP = ifft(dataSymbolsTemp);
    transmitSignal = [TransmitSignalNoCP(:,end-L_cp+1:end),TransmitSignalNoCP];

end

