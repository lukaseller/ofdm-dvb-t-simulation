function s_m = remove_frequency_offset(s, epsilon, K)
%remove_frequency_offset Compensate for a frequency offset of epsilon

[~, N] = size(s);
frequency_shift = exp(-1j*2*pi*epsilon*(1:N)/K);
s_m = s .* frequency_shift;

end

% s_m = zeros(S,N);
% for i=1:S
%     s_m(i,:) = s(i,:).*exp(-1j*2*pi*epsilon*(1:N)/K);
% end