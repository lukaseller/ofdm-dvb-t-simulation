function [theta, epsilon] = time_frequency_offset(r_m, L_cp, K, SNR)
%time_frequency_offset Estimates the time frequency offset using the Cyclic
%Prefix of the OFDM signal

    rho= SNR/(SNR+1);
    Q = K + 2 * L_cp;

    phi = zeros(1,Q);
    psi = zeros(1,Q);
    for m=1:Q
        phi(m) = xcorr( r_m(m:m+L_cp), r_m(m+K:m+L_cp+K), 0);
        psi(m) = energy_term(r_m, m, K, L_cp);
    end

    [~, theta]= max(abs(phi) - rho * psi);
    epsilon = - 1/ (2*pi) * angle(phi(theta));
    %argmax + timing_offset - N 
end



function psi = energy_term(r, m, K, L_cp)
    result = 0;
    for l=m:m+L_cp
        result = result + abs(r(l)^2) + abs(r(l+K)^2);
    end
    psi = 1/2 * result;
end