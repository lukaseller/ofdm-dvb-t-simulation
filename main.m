% ber = [];
% for snr=0:5:5
%     fprintf('SNR: %f db \n', snr);
%     values = [];
%     for i=0:1
%         values = [values, simulate_one_frame(snr)];
%     end
%     ber = [ber, mean(values)];
% end
% 
% figure()
% plot(log10(ber));


% function BER = simulate_one_frame(SNR_db)
% Initialization
    FFTSize         = 8192;                         %FFT Size
    nSubcarriers    = 6817;                         % Number of used Subcarriers
    nSamplesTotal   = 10240;                        %Total Symbol Duration
    lengthCP        = nSamplesTotal - FFTSize;      %Length Cyclic Prefix
    lengthChannel   = lengthCP - 1000;              % 1000; %Length Channel -> shorter than L_cp because of symbol timing offset
    frameLength     = 68;                           %For one Frame
    modulationOrder = 16;                           % QAM modulaton order M
    bitsPerSymbol = log2(modulationOrder);          % Bits per Symbol k
    SNR_db = 100;
    SNR = 10^(SNR_db/10);

    timing_offset = lengthCP + randi([1, ceil(FFTSize/2)], 1, 1); 
    %offset is within limit to be sure we get second symbol (pilot subcarriers)

    % QAM alphabet vector and corresponding Grey Bit mapping matrix
    [QAMAlphabet, bitMask]= mod_demod.SignalConstellation;
    
    %Generate a SxK matrix which indicates the position of the pilots
    [pilotMask, total_data_symbols] = channel_estimation.generate_pilot_mask(frameLength,nSubcarriers);

    % Generate Binary Data Stream & map to QAM symbols
    dataBits = randi([0 1],1,total_data_symbols * bitsPerSymbol);
    txIntegers = 2.^[0:bitsPerSymbol-1]*reshape(dataBits,bitsPerSymbol,[]);   % shape into bitwords
    dataStream = QAMAlphabet(txIntegers+1);
    %Generate a 16QAM data index stream
%     dataStream = randi([0, 15], 1, total_data_symbols); 

    %Assign the symbols from the stream to all free position in the pilot masks
    %and set the pilots to QAM index 8
    dataStreamWithPilots = mod_demod.assign_subcarriers(dataStream, pilotMask, QAMAlphabet(8));
    dataSymbols = mod_demod.modulate_ofdm(dataStreamWithPilots, lengthCP, FFTSize);
    
    epsilon = 0.02;
    dataSymbols = synchronisation.add_frequency_offset(dataSymbols, epsilon, FFTSize);

    %Generate S channel realizations and convolve each symbol with the
    %corresponding one
    H_l = channel_estimation.test_channel_matrix(frameLength,lengthChannel);
    r_m = channel_estimation.convolve_and_noise(dataSymbols, H_l, SNR);

    %%Receiver

    %add the random timing offset
    r_m = r_m(1,timing_offset:end); 


    [estimated_offset, frequency_offset] = synchronisation.timing_offset(r_m, lengthCP, FFTSize, SNR);
    offset_error = estimated_offset + timing_offset - nSamplesTotal;
    %In most cases the offset error is positive
    
    fprintf('Timing Offset Error: %f \n', offset_error);
    fprintf('Frequency Offset: %f \n', frequency_offset);
    if offset_error > 100 || offset_error < -100
        BER = 1;
        fprintf('Timing Synchronisation failed \n');
        return 
    end
    
    %Timing offset error needs to be smaller than 200 symbols otherwhise the
    %offset cannot be compensated

    %Discard the first symbol -> due to the offset estimation
    frameLength = frameLength - 1;
    pilotMask = pilotMask(2:end,:);
    r_m = r_m(1,estimated_offset:end);
    
    

    %Back in Matrix form
    r =  transpose(reshape(r_m(1,1:nSamplesTotal*frameLength), [nSamplesTotal,frameLength]));

    r = synchronisation.remove_frequency_offset(r, frequency_offset, FFTSize);
    
    %OFDM Demodulation
    y = fft(r(:,lengthCP+1:end));
    y = y(:,1:nSubcarriers);

    % Channel Estimation
    estimated_channel = channel_estimation.estimate_channel_matrix(y, pilotMask, frameLength, nSubcarriers, lengthChannel);

    %Equalization
    s_est = (y./estimated_channel).';
    s_est = s_est(~pilotMask.');
    
    % Minimum Distance Demapping 
    [~, rxSymbolsSorted] = min(abs(repmat(s_est, 1, modulationOrder)-repmat(QAMAlphabet, length(s_est), 1)), [], 2);
    rxBits = bitMask(:,rxSymbolsSorted);
    x_est_prime = rxBits(:);
    
    %Remove pilots from original signal and from estimated
    x_prime = dataStreamWithPilots(2:end,:);
    x_prime = x_prime(pilotMask == 0);

    BER = 1 - sum(x_est_prime == x_prime) / (total_data_symbols - (nSubcarriers - sum(pilotMask(1,:))));
    fprintf('BER: %f \n', BER);
% end
