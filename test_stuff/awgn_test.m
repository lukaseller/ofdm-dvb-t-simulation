SNR_db = 50;%
SNR = 10^(SNR_db/10);

x_stream = randi([0, 15], 1, 10000);
x = qammod(x(i,:), 16)./sqrt(10);
 

r = x + 1/sqrt(SNR) * (normrnd(0,1/sqrt(2),[1,10000]) + 1j * normrnd(0,1/sqrt(2), [1,10000]));