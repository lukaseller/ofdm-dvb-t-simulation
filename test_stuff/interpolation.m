


K = 5;
x = [1,nan,1*exp(1j*2*pi*3/K),nan,1];
%x(imag(x)~=0) = nan;
nanx = isnan(x);

t = 1:K;

y = interp1(t(~nanx), x(~nanx), 1:K,'spline');


figure(1)
hold on;
stem(imag(y)) %the phase might be correct? 
stem(real(y))
hold off;

abs(1*exp(1j*2*pi*3/K))
%phase is actually interpolated linearly
