
%K = 5;
%x = [1,nan,0,nan,1];
%nanx = isnan(x);
%x(nanx) = interp1(t(~nanx), x(~nanx), 1:K);
%stem(x)
%conv(  [1,2,4], [1,2])


h = abs((normrnd(0,1/sqrt(2),[1,10000]) + 1j * normrnd(0,1/sqrt(2), [1,10000])).^2);
mean(h);

s = abs((qammod(randi([0, 15], 1, 1000), 16)./sqrt(10)).^2);
mean(s);


SNR_db = 50;%
SNR = 10^(SNR_db/10);

noise = 1/sqrt(SNR) * (normrnd(0,1/sqrt(2),[1,10000]) + 1j * normrnd(0,1/sqrt(2), [1,10000]));
signal = qammod(randi([0, 15], 1, 1000), 16)./sqrt(10);

SNR_est = mean(abs(signal).^2)/(mean(abs(noise).^2));
SNR_est_db = 10*log10(SNR_est);


